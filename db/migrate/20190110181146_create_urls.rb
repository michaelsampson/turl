class CreateUrls < ActiveRecord::Migration[5.1]
  def change
    create_table :urls do |t|
      t.references :user, :null => false
      t.string :long_url
      t.string :short_url_token
      t.integer :hit_count
      t.timestamps
    end
  end
end
