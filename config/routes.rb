Rails.application.routes.draw do
  root to: 'urls#index'
  resources :urls, only: [:index, :create]
  devise_for :users
  get "t/:id" => "urls#redirect"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
