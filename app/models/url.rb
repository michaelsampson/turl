class Url < ApplicationRecord
  belongs_to :user
  validates :long_url, presence: true, url: true
  validates :short_url_token, presence: true
  validates :user, presence: true

  def self.generate_short_url_token
    length_in_bytes = SecureRandom.random_number(3) + 4
    short_token = SecureRandom.urlsafe_base64(length_in_bytes, false)
    if Url.find_by(short_url_token: short_token).present?
      self.generate_short_url_token
    end
    short_token
  end
end
