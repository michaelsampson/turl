class UrlsController < ApplicationController
  before_action :authenticate_user!

  # GET /urls
  # GET /urls.json
  def index
    @url = Url.new
    @urls = Url.where(user: current_user)
  end

  # POST /urls
  # POST /urls.json
  def create
    @url = Url.new(url_params)
    @url.user = current_user
    @url.short_url_token = Url.generate_short_url_token
    @url.hit_count = 0

    respond_to do |format|
      if @url.save
        format.html { redirect_to '/', notice: 'Url was successfully created.' }
      else
        @urls = Url.where(user: current_user)
        format.html { render :index }
      end
    end
  end

  def redirect
    short_url_token = params[:id]
    url = Url.find_by(short_url_token: short_url_token)
    if url.present?
      url.update(hit_count: url.hit_count + 1)
      redirect_to url.long_url
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def url_params
      params[:url].permit(:long_url)
    end
end
