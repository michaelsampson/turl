```
git clone git@bitbucket.org:michaelsampson/turl.git
cd turl
bundle install
rails db:setup
rails s
```

See seeds.rb for a test user or create your own with sign up form.
